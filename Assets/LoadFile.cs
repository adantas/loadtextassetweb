﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoadFile : MonoBehaviour
{
    public Text Output;
    public TextAsset TextAsset;

    public void Start()
    {
        Output.text = TextAsset.text;
    }
}
